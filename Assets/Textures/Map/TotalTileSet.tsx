<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="TotalTileSet" tilewidth="16" tileheight="16" tilecount="520" columns="10">
 <image source="Forest_v2.png" width="160" height="832"/>
 <tile id="60">
  <objectgroup draworder="index" id="2">
   <object id="4" x="6.375" y="5.625">
    <polygon points="0,10.375 9.5,10.375 9.625,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="61">
  <objectgroup draworder="index" id="2">
   <object id="4" x="0.25" y="6.125">
    <polygon points="-0.125,0 15.625,0.125 15.75,9.75 -0.25,9.625"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="62">
  <objectgroup draworder="index" id="2">
   <object id="4" x="0" y="5.375">
    <polygon points="0.25,0.5 10.25,10.375 0.125,10.625"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="63">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="64">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="70">
  <objectgroup draworder="index" id="2">
   <object id="3" x="6.875" y="0.25">
    <polygon points="0,0 0.125,15.5 9.25,15.625 9.25,-0.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="71">
  <animation>
   <frame tileid="71" duration="333"/>
   <frame tileid="83" duration="333"/>
   <frame tileid="84" duration="333"/>
  </animation>
 </tile>
 <tile id="72">
  <objectgroup draworder="index" id="2">
   <object id="7" x="9.25" y="0">
    <polygon points="0,0 0.625,15.875 -9,15.875 -9,0.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="73">
  <objectgroup draworder="index" id="2">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="74">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="80">
  <objectgroup draworder="index" id="2">
   <object id="4" x="6.25" y="0">
    <polygon points="0,0 9.5,0 9.625,10.375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="81">
  <objectgroup draworder="index" id="2">
   <object id="5" x="0.25" y="9.125">
    <polygon points="0,-0.125 15.5,0.125 15.625,-9.125 -0.375,-9.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="82">
  <objectgroup draworder="index" id="2">
   <object id="5" x="9.875" y="10.625" rotation="180">
    <polygon points="0,10.375 9.5,10.375 9.625,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="83">
  <animation>
   <frame tileid="83" duration="333"/>
   <frame tileid="84" duration="333"/>
   <frame tileid="71" duration="333"/>
  </animation>
 </tile>
 <tile id="84">
  <animation>
   <frame tileid="84" duration="333"/>
   <frame tileid="71" duration="333"/>
   <frame tileid="83" duration="333"/>
  </animation>
 </tile>
 <tile id="90">
  <objectgroup draworder="index" id="2">
   <object id="2" x="5.75" y="16">
    <polygon points="0,0 4,-6.25 10.125,-10.125 10.25,-0.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="91">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="5.75" width="16" height="10.25"/>
  </objectgroup>
 </tile>
 <tile id="92">
  <objectgroup draworder="index" id="2">
   <object id="1" x="10.125" y="5.625" rotation="180">
    <polygon points="0,-10.625 2.80488,-2.56636 9.7561,0 9.75,-10.3627"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="100">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.625" y="0" width="10.375" height="16"/>
  </objectgroup>
 </tile>
 <tile id="102">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="10.5" height="16"/>
  </objectgroup>
 </tile>
 <tile id="110">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5.625" y="0.375">
    <polygon points="-0.625,-0.25 10,10.5 10.125,-0.375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="111">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="10"/>
  </objectgroup>
 </tile>
 <tile id="112">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.375" y="10.625">
    <polygon points="-0.5,0.125 10.875,-10.5 -0.375,-10.5"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="125">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="126">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="127">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="135">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="137">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="145">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="146">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="147">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="165">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4.125" y="0.0625" width="7.75" height="11.125"/>
  </objectgroup>
 </tile>
 <tile id="185">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3.375" y="2.625" width="11" height="9.5">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="217">
  <objectgroup draworder="index" id="2">
   <object id="4" x="0" y="6.875" width="16" height="9.125"/>
  </objectgroup>
 </tile>
 <tile id="218">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="7.625" width="16" height="8.375"/>
  </objectgroup>
 </tile>
 <tile id="219">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="8.375" width="16" height="7.625"/>
  </objectgroup>
 </tile>
 <tile id="227">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="228">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="229">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="230">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6.125" y="15.75">
    <polygon points="0,0 9.625,-6.5 9.75,0.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="231">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.25" y="9.75">
    <polygon points="0,0 15.625,0.25 15.75,6.25 -0.25,6.375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="232">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0.25" y="9">
    <polygon points="0,0 7,6.875 -0.125,6.875"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="235">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6.875" y="15.5">
    <polygon points="-0.125,0.5 9,-2.125 9,0.5"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="236">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.125" y="12.5">
    <polygon points="0,0 9.5,3.5 -0.125,3.5"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="237">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="238">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="239">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="240">
  <objectgroup draworder="index" id="2">
   <object id="2" x="5" y="0.125">
    <polygon points="0,0 10.75,13.375 11,-0.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="241">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="15"/>
  </objectgroup>
 </tile>
 <tile id="242">
  <objectgroup draworder="index" id="2">
   <object id="2" x="8.125" y="0.125">
    <polygon points="0,0 -8,13.25 -8,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="243">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2" y="0" width="14" height="15"/>
  </objectgroup>
 </tile>
 <tile id="244">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="13" height="15"/>
  </objectgroup>
 </tile>
 <tile id="245">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5" y="0">
    <polygon points="0,0 3.5,8 10.75,11.625 10.875,0.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="246">
  <objectgroup draworder="index" id="2">
   <object id="2" x="12.75" y="0.375">
    <polygon points="0,0 -4.25,6.5 -12.875,11.625 -12.75,-0.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="247">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="11"/>
  </objectgroup>
 </tile>
 <tile id="248">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="12"/>
  </objectgroup>
 </tile>
 <tile id="249">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="11"/>
  </objectgroup>
 </tile>
 <tile id="274">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6" y="10.25" width="10" height="5.75"/>
  </objectgroup>
 </tile>
 <tile id="275">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="11" width="9" height="5"/>
  </objectgroup>
 </tile>
 <tile id="281">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.125" y="7.375" width="16" height="8.5"/>
  </objectgroup>
 </tile>
 <tile id="282">
  <objectgroup draworder="index" id="2">
   <object id="4" x="0" y="7.5" width="16" height="8.5"/>
  </objectgroup>
 </tile>
 <tile id="284">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0" width="12" height="10"/>
  </objectgroup>
 </tile>
 <tile id="285">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="12" height="11"/>
  </objectgroup>
 </tile>
 <tile id="291">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0.375" y="0.125">
    <polygon points="0,0 15.375,13.75 15.375,0.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="292">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0.125" y="13.75">
    <polygon points="0,0 15.625,-13.625 -0.125,-13.75"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="294">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5" y="4.75" width="11" height="10.25"/>
  </objectgroup>
 </tile>
 <tile id="295">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4" width="8" height="10"/>
  </objectgroup>
 </tile>
 <tile id="357">
  <objectgroup draworder="index" id="2">
   <object id="6" x="0" y="0" width="16" height="15"/>
  </objectgroup>
 </tile>
 <tile id="358">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7" y="0" width="9" height="15"/>
  </objectgroup>
 </tile>
 <tile id="359">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="9" height="15"/>
  </objectgroup>
 </tile>
</tileset>
