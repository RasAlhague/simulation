﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float cameraUpDownSped = 5f;
    public Rigidbody2D rb;
    public CinemachineVirtualCamera usedCamera;

    private Vector2 movement;
    private bool up;
    private bool down;

    // Update is called once per frame
    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        if (Input.GetKey(KeyCode.KeypadPlus) && usedCamera.m_Lens.OrthographicSize > 5)
        {
            usedCamera.m_Lens.OrthographicSize -= cameraUpDownSped * Time.fixedDeltaTime;
        }
        if (Input.GetKey(KeyCode.KeypadMinus) && usedCamera.m_Lens.OrthographicSize < 30)
        {
            usedCamera.m_Lens.OrthographicSize += cameraUpDownSped * Time.fixedDeltaTime;
        }

        if (usedCamera.m_Lens.OrthographicSize < 5)
        {
            usedCamera.m_Lens.OrthographicSize = 5;
        }
        if (usedCamera.m_Lens.OrthographicSize > 30)
        {
            usedCamera.m_Lens.OrthographicSize = 30;
        }
    }
}
