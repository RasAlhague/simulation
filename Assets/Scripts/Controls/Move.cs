﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Move : MonoBehaviour
{
    private Vector2 movement;
    private Transform closestFood;
    private Transform closestWater;
    private float elapsedTime;
    private Slime slime;
    private Hunger hunger;
    private Thirst thirst;
    private Reproduction reproduction;
    private Mating mating;
    private bool hungry;
    private bool thirsty;

    public float moveDuration = 0.5f;
    public float moveSpeed = 1f;
    public Rigidbody2D rb;

    void Start()
    {
        elapsedTime = moveDuration;
        slime = GetComponent<Slime>();
        hunger = GetComponent<Hunger>();
        thirst = GetComponent<Thirst>();
        reproduction = GetComponent<Reproduction>();
        mating = GetComponent<Mating>();
    }

    void Update()
    {
        if (elapsedTime <= 0)
        {
            movement.x = Random.Range(-1f, 1f);
            movement.y = Random.Range(-1f, 1f);
            elapsedTime = moveDuration;
        }

        hungry = hunger.totalHunger * (hunger.hungerThreshold / 100) > hunger.currentHunger;
        thirsty = thirst.totalThrist * (thirst.thirstThreshold / 100) > thirst.currentThrist;

        elapsedTime -= Time.deltaTime;
    }

    void FixedUpdate()
    {
        if (hungry)
        {
            closestFood = SearchForFood(transform.position, slime.sensoryWidth);
            if (closestFood != null)
            {
                MoveTowardsFood();
                return;
            }
        }
        if (thirsty)
        {
            closestWater = SearchForWater(transform.position, slime.sensoryWidth);
            if (closestWater != null)
            {
                MoveTowardsWater();
                return;
            }
        }
        if (mating.hasMatingUrge && mating.partnerComp != null)
        {
            MoveTowardsMates();
            return;
        }

        rb.MovePosition((moveSpeed * Time.fixedDeltaTime) * movement.normalized + rb.position);

    }

    private void MoveTowardsFood()
    {
        Vector2 direction = closestFood.position - transform.position;
        Vector2 foodPos = closestFood.position;

        Debug.DrawLine(rb.position, foodPos, Color.green, Time.fixedDeltaTime, false);
        rb.MovePosition((moveSpeed * Time.fixedDeltaTime) * direction.normalized + rb.position);
    }

    private void MoveTowardsMates()
    {
        Vector2 direction = mating.partnerComp.transform.position - transform.position;
        Vector2 closestPos = mating.partnerComp.transform.position;

        Debug.DrawLine(rb.position, closestPos, Color.blue, Time.fixedDeltaTime, false);
        rb.MovePosition((moveSpeed * Time.fixedDeltaTime) * direction.normalized + rb.position);
    }

    private void MoveTowardsWater()
    {
        Vector2 direction = closestWater.position - transform.position;
        Vector2 waterPos = closestWater.position;

        Debug.DrawLine(rb.position, waterPos, Color.red, Time.fixedDeltaTime, false);
        rb.MovePosition((moveSpeed * Time.fixedDeltaTime) * direction.normalized + rb.position);
    }

    private Transform SearchForWater(Vector2 center, float radius)
    {
        LayerMask mask = LayerMask.GetMask("Water");
        Collider2D hitCollider = Physics2D.OverlapCircle(center, radius, mask);

        if (hitCollider != null && hitCollider.gameObject.activeInHierarchy)
        {
            return hitCollider.transform;
        }

        return null;
    }

    private Transform SearchForFood(Vector2 center, float radius)
    {
        LayerMask mask = LayerMask.GetMask("Plant");
        Collider2D hitCollider = Physics2D.OverlapCircle(center, radius, mask);

        if (hitCollider != null && hitCollider.gameObject.activeInHierarchy)
        {
            return hitCollider.transform;
        }

        return null;
    }
}
