﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectSlime : MonoBehaviour
{
    public GameObject clickedSlime;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            var mask = LayerMask.GetMask("Slimes");

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero, mask);

            if (hit.collider != null)
            {
                clickedSlime = hit.collider.gameObject;
            }
            else
            {
                clickedSlime = null;
            }
        }

        if (clickedSlime != null)
        {
            gameObject.transform.position = new Vector2(clickedSlime.transform.position.x, clickedSlime.transform.position.y);
        }
    }
}
