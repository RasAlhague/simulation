﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public float nutritionalValue = 10;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Slime")
        {
            collision.SendMessage("DecreaseHunger", nutritionalValue);
            Destroy(gameObject);
        }
    }
}
