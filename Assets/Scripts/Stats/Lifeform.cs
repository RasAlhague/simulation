﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifeform : MonoBehaviour
{
    public List<GenomeBase> genomes = new List<GenomeBase>();
    public int generation = 1;
}
