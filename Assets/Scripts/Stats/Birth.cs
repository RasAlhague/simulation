﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Birth : MonoBehaviour
{
    public int offspringNumbers = 4;
    public float gestationTime = 5;
    public Sprite pregSprite;
    public Sprite normalSprite;

    private GameObject maleTemplate;
    private GameObject femaleTemplate;
    private GameObject tempPartnerObject;
    private bool canBirth;
    private float internalTimer;
    private Slime ownSlimeComp;
    private SpriteRenderer _renderer;

    public void Start()
    {
        maleTemplate = Resources.Load("Prefabs/Slime") as GameObject;
        femaleTemplate = Resources.Load("Prefabs/SlimeFemale") as GameObject;
        internalTimer = gestationTime;
        canBirth = false;
        ownSlimeComp = gameObject.GetComponent<Slime>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (ownSlimeComp.gender == Assets.Scripts.Gender.Male)
        {
            return;
        }

        if (internalTimer <= 0)
        {
            if (canBirth)
            {
                GiveBirth();

                Destroy(tempPartnerObject);
                tempPartnerObject = null;

                canBirth = false;
            }
            internalTimer = gestationTime;
        }
        internalTimer -= Time.deltaTime;
    }

    public void BirthSlimes(Slime partner)
    {
        if (ownSlimeComp.gender == Assets.Scripts.Gender.Male)
        {
            return;
        }

        canBirth = true;

        if (tempPartnerObject != null)
        {
            Destroy(tempPartnerObject);
        }

        tempPartnerObject = new GameObject($"temp_{ownSlimeComp.id}{partner.id}");

        foreach (var genome in partner.GetComponents<GenomeBase>())
        {
            var gen = (GenomeBase)tempPartnerObject.AddComponent(genome.GetType());
            gen.GetValueFromOther(genome);
        }

        if (pregSprite != null)
        {
            _renderer.sprite = pregSprite;
        }
    }

    private void GiveBirth()
    {
        var currentSlime = GetComponent<Slime>();

        if (currentSlime.gender == Assets.Scripts.Gender.Male)
        {
            return;
        }

        var ownGenomes = GetComponents<GenomeBase>();

        var totalGenomes = new List<GenomeBase>();
        totalGenomes.AddRange(ownGenomes);
        totalGenomes.AddRange(tempPartnerObject.gameObject.GetComponents<GenomeBase>());
        Shuffle(totalGenomes);
        totalGenomes.RemoveRange(totalGenomes.Count / 2, totalGenomes.Count / 2);

        for (int i = 0; i < offspringNumbers; i++)
        {
            GameObject go;
            if (Random.Range(0, 100) > 50)
            {
                go = Instantiate(maleTemplate, new Vector3(transform.position.x + Random.Range(-0.5f, 0.5f), transform.position.y + Random.Range(-0.5f, 0.5f), 0), Quaternion.identity);
            }
            else
            {
                go = Instantiate(femaleTemplate, new Vector3(transform.position.x + Random.Range(-0.5f, 0.5f), transform.position.y + Random.Range(-0.5f, 0.5f), 0), Quaternion.identity);
            }
            var lifeform = go.GetComponent<Lifeform>();
            lifeform.generation = GetComponent<Lifeform>().generation + 1;

            foreach (var genome in totalGenomes)
            {
                var comp = (GenomeBase)go.AddComponent(genome.GetType());
                comp.GetValueFromOther(genome);

                if (Random.Range(0, 1000) < 10)
                {
                    comp.RandomizeValues();
                }

                lifeform.genomes.Add(comp);
            }
        }

        if (normalSprite != null)
        {
            _renderer.sprite = normalSprite;
        }
    }

    public void Shuffle(List<GenomeBase> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n);
            GenomeBase value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
