﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thirst : MonoBehaviour
{
    public float totalThrist = 100f;
    public float currentThrist = 25f;
    public float consumptionRate = 1f;
    public float interval = 1f;
    public float thirstThreshold = 30;

    // Update is called once per frame
    IEnumerator Start()
    {
        yield return StartCoroutine(WaitAndReduce());
    }

    IEnumerator WaitAndReduce()
    {
        while (true)
        {
            currentThrist -= consumptionRate;
            yield return new WaitForSecondsRealtime(interval);
        }      
    }

    void Update()
    {
        if (currentThrist <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void DecreaseThirst(float value)
    {
        currentThrist += value;
        if (currentThrist > totalThrist)
        {
            currentThrist = totalThrist;
        }
    }
}
