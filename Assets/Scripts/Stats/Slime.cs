﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Slime : MonoBehaviour
{
    public float sensoryWidth = 0.6f;
    public Guid id;
    public Gender gender;

    void Start()
    {
        id = Guid.NewGuid();
        var lifeform = gameObject.GetComponent<Lifeform>();

        foreach (var genome in lifeform.genomes)
        {
            genome.ApplyGenom(gameObject);
        }
    }
}
