﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reproduction : MonoBehaviour
{
    public float total = 100;
    public float reproductiveUrge = 0;
    public float increasingRate = 1f;
    public float interval = 1f;
    public float urgeThreshold = 50;

    IEnumerator Start()
    {
        yield return StartCoroutine(WaitAndIncrease());
    }

    IEnumerator WaitAndIncrease()
    {
        while (true)
        {
            reproductiveUrge += increasingRate;
            yield return new WaitForSecondsRealtime(interval);
        }
    }

    public void ResetUrge()
    {
        reproductiveUrge = 0;
    }
}
