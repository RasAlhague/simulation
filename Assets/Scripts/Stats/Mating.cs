﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Mating : MonoBehaviour
{
    public Slime slimeComp;
    public Slime partnerComp;
    public float griefanceTime = 10;
    public bool hasMatingUrge;

    private Reproduction reproductionComp;
    private Queue<Slime> unwillingSlimes;
    private float forgetMateTime;

    // Start is called before the first frame update
    void Start()
    {
        unwillingSlimes = new Queue<Slime>();
        reproductionComp = gameObject.GetComponent<Reproduction>();
        slimeComp = gameObject.GetComponent<Slime>();
        forgetMateTime = griefanceTime;
    }

    // Update is called once per frame
    void Update()
    {
        hasMatingUrge = reproductionComp.total * (reproductionComp.urgeThreshold / 100) < reproductionComp.reproductiveUrge;
    }

    private void FixedUpdate()
    {
        if (forgetMateTime <= 0)
        {
            if (unwillingSlimes.Any())
            {
                unwillingSlimes.Dequeue();
            }
            forgetMateTime = griefanceTime;
        }

        forgetMateTime -= Time.fixedDeltaTime;

        if(!hasMatingUrge && partnerComp != null)
        {
            partnerComp = null;
        }

        if (partnerComp == null && hasMatingUrge)
        {
            var closestFoundSlime = SearchForMates(transform.position, slimeComp.sensoryWidth);

            if (closestFoundSlime == null)
            {
                return;
            }

            var otherMating = closestFoundSlime.GetComponent<Mating>();
            if (otherMating.ReactOnAllure(slimeComp))
            {
                partnerComp = closestFoundSlime;
            }
            else
            {
                unwillingSlimes.Enqueue(closestFoundSlime);
            }
        }
    }

    private Slime SearchForMates(Vector2 center, float radius)
    {
        LayerMask mask = LayerMask.GetMask("Slimes");
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(center, radius, mask);

        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider != null && hitCollider.gameObject.activeInHierarchy)
            {
                var hitSlimeComp = hitCollider.gameObject.GetComponent<Slime>();

                if (unwillingSlimes.Any(x => x.id == hitSlimeComp.id))
                {
                    return null;
                }

                if (hitSlimeComp.gender == slimeComp.gender)
                {
                    return null;
                }

                return hitSlimeComp;
            }
        }

        return null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.CompareTag("Slime"))
        {
            return;
        }
        if (partnerComp == null)
        {
            return;
        }

        var slimeOther = collision.gameObject.GetComponent<Slime>();
        if (slimeOther.id != partnerComp.id)
        {
            return;
        }

        var reproductionOther = partnerComp.gameObject.GetComponent<Reproduction>();
        reproductionOther.SendMessage("ResetUrge");

        if (slimeComp.gender == Assets.Scripts.Gender.Female)
        {
            SendMessage("BirthSlimes", partnerComp);
        }
        else
        {
            partnerComp.SendMessage("BirthSlimes", slimeComp);
        }

        partnerComp = null;
    }

    public bool ReactOnAllure(Slime newPartner)
    {
        if (!hasMatingUrge)
        {
            return false;
        }
        if (Random.Range(0, 100) < 25)
        {
            return false;
        }
        if (unwillingSlimes.Any(x => x.id == newPartner.id))
        {
            return false;
        }

        partnerComp = newPartner;
        return true;
    }

}
