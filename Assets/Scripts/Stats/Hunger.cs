﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hunger : MonoBehaviour
{
    public float totalHunger = 100f;
    public float currentHunger = 25f;
    public float consumptionRate = 1f;
    public float interval = 1f;
    public float hungerThreshold = 25f;

    // Update is called once per frame
    IEnumerator Start()
    {
        yield return StartCoroutine(WaitAndReduce());
    }

    IEnumerator WaitAndReduce()
    {
        while (true)
        {
            currentHunger -= consumptionRate;
            yield return new WaitForSecondsRealtime(interval);
        }
    }

    void Update()
    {
        if(currentHunger <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void DecreaseHunger(float value)
    {
        currentHunger += value;
        if (currentHunger > totalHunger)
        {
            currentHunger = totalHunger;
        }
    }
}
