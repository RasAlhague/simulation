﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HungerDropSpeedGenome : GenomeBase
{

        public float totalHungerModifier = 1f;
        public float consumptionRateModifier = 1f;

    public override void ApplyGenom(GameObject gameObject)
    {
        var hunger = gameObject.GetComponent<Hunger>();

        if (hunger != null)
        {
            hunger.totalHunger *= totalHungerModifier;
            hunger.consumptionRate *= consumptionRateModifier;
        }
    }

    public override void GetValueFromOther(GenomeBase genomeBase)
    {
        var gen = genomeBase as HungerDropSpeedGenome;

        if(gen == null)
        {
            throw new ArgumentException("Invalid genome for this type!");
        }

        totalHungerModifier = gen.totalHungerModifier;
        consumptionRateModifier = gen.consumptionRateModifier;
    }

    public override void RandomizeValues()
    {
        totalHungerModifier = UnityEngine.Random.Range(0.5f, 1.5f);
        consumptionRateModifier = UnityEngine.Random.Range(0.5f, 1.5f);
    }
}
