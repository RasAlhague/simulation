﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirstDropSpeedGenome : GenomeBase
{
    public float totalthirstModifier = 1f;
    public float consumptionRateModifier = 1f;

    public override void ApplyGenom(GameObject gameObject)
    {
        var thirst = gameObject.GetComponent<Thirst>();

        if (thirst != null)
        {
            thirst.totalThrist *= totalthirstModifier;
            thirst.consumptionRate *= consumptionRateModifier;
        }
    }

    public override void GetValueFromOther(GenomeBase genomeBase)
    {
        var gen = genomeBase as ThirstDropSpeedGenome;

        if (gen == null)
        {
            throw new ArgumentException("Invalid genome for this type!");
        }

        consumptionRateModifier = gen.consumptionRateModifier;
        totalthirstModifier = gen.totalthirstModifier;
    }

    public override void RandomizeValues()
    {
        totalthirstModifier = UnityEngine.Random.Range(0.5f, 1.5f);
        consumptionRateModifier = UnityEngine.Random.Range(0.5f, 1.5f);
    }
}

