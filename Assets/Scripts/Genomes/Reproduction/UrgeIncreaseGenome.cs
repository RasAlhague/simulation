﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrgeIncreaseGenome : GenomeBase
{
    public float urgeIncrease = 1f;

    public override void ApplyGenom(GameObject gameObject)
    {
        var reproductionComp = gameObject.GetComponent<Reproduction>();

        if (reproductionComp != null)
        {
            reproductionComp.increasingRate *= urgeIncrease;
        }
    }

    public override void GetValueFromOther(GenomeBase genomeBase)
    {
        var gen = genomeBase as UrgeIncreaseGenome;

        if (gen == null)
        {
            throw new ArgumentException("Invalid genome for this type!");
        }

        urgeIncrease = gen.urgeIncrease;
    }

    public override void RandomizeValues()
    {
        urgeIncrease = UnityEngine.Random.Range(0.75f, 1.25f);
    }

}
