﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrgeThresholdGenome : GenomeBase
{
    public float urgeThreshold = 1f;

    public override void ApplyGenom(GameObject gameObject)
    {
        var reproductionComp = gameObject.GetComponent<Reproduction>();

        if (reproductionComp != null)
        {
            reproductionComp.urgeThreshold *= urgeThreshold;
        }
    }

    public override void GetValueFromOther(GenomeBase genomeBase)
    {
        var gen = genomeBase as UrgeThresholdGenome;

        if (gen == null)
        {
            throw new ArgumentException("Invalid genome for this type!");
        }

        urgeThreshold = gen.urgeThreshold;
    }

    public override void RandomizeValues()
    {
        urgeThreshold = UnityEngine.Random.Range(0.75f, 1.25f);
    }
}
