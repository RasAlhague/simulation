﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffspringNumberGenome : GenomeBase
{
    public int offsprings = 4;
    public int maxOffSprings = 6;

    public override void ApplyGenom(GameObject gameObject)
    {
        var birthComp = gameObject.GetComponent<Birth>();

        if (birthComp != null)
        {
            birthComp.offspringNumbers = offsprings;
        }
    }

    public override void GetValueFromOther(GenomeBase genomeBase)
    {
        var gen = genomeBase as OffspringNumberGenome;

        if (gen == null)
        {
            throw new ArgumentException("Invalid genome for this type!");
        }

        offsprings = gen.offsprings;
        maxOffSprings = gen.maxOffSprings;
    }

    public override void RandomizeValues()
    {
        offsprings = UnityEngine.Random.Range(1, maxOffSprings + 1);
    }
}
