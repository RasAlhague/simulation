﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestationTimeGenome : GenomeBase
{
    public float gestationTime = 5f;

    public override void ApplyGenom(GameObject gameObject)
    {
        var birthComp = gameObject.GetComponent<Birth>();

        if (birthComp != null)
        {
            birthComp.gestationTime *= gestationTime;
        }
    }

    public override void GetValueFromOther(GenomeBase genomeBase)
    {
        var gen = genomeBase as GestationTimeGenome;

        if (gen == null)
        {
            throw new ArgumentException("Invalid genome for this type!");
        }

        gestationTime = gen.gestationTime;
    }

    public override void RandomizeValues()
    {
        gestationTime = UnityEngine.Random.Range(0.5f, 1.5f);
    }
}
