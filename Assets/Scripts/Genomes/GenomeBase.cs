﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenomeBase : MonoBehaviour, IGenome
{
    public abstract void ApplyGenom(GameObject gameObject);
    public abstract void GetValueFromOther(GenomeBase genomeBase);
    public abstract void RandomizeValues();
}
