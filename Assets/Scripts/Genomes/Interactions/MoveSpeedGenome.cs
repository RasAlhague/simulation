﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpeedGenome : GenomeBase
{
    public float durationModifier = 1f;
    public float speedModifier = 1f;

    public override void ApplyGenom(GameObject gameObject)
    {
        var move = gameObject.GetComponent<Move>();

        if(move != null)
        {
            move.moveSpeed *= speedModifier;
            move.moveDuration *= durationModifier;
        }
    }

    public override void GetValueFromOther(GenomeBase genomeBase)
    {
        var gen = genomeBase as MoveSpeedGenome;

        if (gen == null)
        {
            throw new ArgumentException("Invalid genome for this type!");
        }

        durationModifier = gen.durationModifier;
        speedModifier = gen.speedModifier;
    }

    public override void RandomizeValues()
    {
        durationModifier = UnityEngine.Random.Range(0.5f, 1.5f);
        speedModifier = UnityEngine.Random.Range(0.5f, 1.5f);
    }
}
