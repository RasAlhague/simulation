﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensoryReachGenome : GenomeBase
{
    public float sensoryWidthModifier = 1f;

    public override void ApplyGenom(GameObject gameObject)
    {
        var slime = gameObject.GetComponent<Slime>();

        if (slime != null)
        {
            slime.sensoryWidth *= sensoryWidthModifier;
        }
    }

    public override void GetValueFromOther(GenomeBase genomeBase)
    {
        var gen = genomeBase as SensoryReachGenome;

        if (gen == null)
        {
            throw new ArgumentException("Invalid genome for this type!");
        }

        sensoryWidthModifier = gen.sensoryWidthModifier;
    }

    public override void RandomizeValues()
    {
        sensoryWidthModifier = UnityEngine.Random.Range(0.5f, 1.5f);
    }
}