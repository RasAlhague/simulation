﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplaySlimeStats : MonoBehaviour
{
    public SelectSlime selectSlime;
    public GameObject slimeInfoUi;
    
    void Update()
    {
        if (selectSlime.clickedSlime != null && !slimeInfoUi.activeInHierarchy)
        {
            slimeInfoUi.SetActive(true);
        } 
        
        if (selectSlime.clickedSlime == null && slimeInfoUi.activeInHierarchy)
        {
            slimeInfoUi.SetActive(false);
        }
    }
}
