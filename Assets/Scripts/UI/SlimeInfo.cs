﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlimeInfo : MonoBehaviour
{
    public SelectSlime selectSlime;

    public Text generationLabel;
    public Text hungerLabel;
    public Text thirstLabel;
    public Text moveSpeedLabel;
    public Text sensoryWidthLabel;
    public Text reproductionLabel;
    public Text offspringsLabel;
    public Text actionLabel;

    // Update is called once per frame
    void Update()
    {
        if (selectSlime.clickedSlime != null)
        {
            var clicked = selectSlime.clickedSlime;

            var lifeformComp = clicked.GetComponent<Lifeform>();
            var hungerComp = clicked.GetComponent<Hunger>();
            var thirstComp = clicked.GetComponent<Thirst>();
            var moveSpeedComp = clicked.GetComponent<Move>();
            var slimeComp = clicked.GetComponent<Slime>();
            var reproductionComp = clicked.GetComponent<Reproduction>();

            string offspringText = "-";

            if (slimeComp.gender == Assets.Scripts.Gender.Female)
            {
                var birth = clicked.GetComponent<Birth>();
                offspringText = birth.offspringNumbers.ToString();
            }

            generationLabel.text = $"Generation: {lifeformComp.generation}";
            hungerLabel.text = $"Hunger: {string.Format("{0:0.00}", hungerComp.currentHunger)}/{string.Format("{0:0.00}", hungerComp.totalHunger)}";
            thirstLabel.text = $"Thirst: {string.Format("{0:0.00}", thirstComp.currentThrist)}/{string.Format("{0:0.00}", thirstComp.totalThrist)}";
            moveSpeedLabel.text = $"Move Speed: {string.Format("{0:0.00}", moveSpeedComp.moveSpeed)}";
            sensoryWidthLabel.text = $"Sensory Width: {string.Format("{0:0.00}", slimeComp.sensoryWidth)}";
            reproductionLabel.text = $"Sex Urge: {string.Format("{0:0.00}", reproductionComp.reproductiveUrge)}/{string.Format("{0:0.00}", reproductionComp.total)}";
            offspringsLabel.text = $"Offsprings: {offspringText}";
            actionLabel.text = $"Action: -";
        }
    }
}
