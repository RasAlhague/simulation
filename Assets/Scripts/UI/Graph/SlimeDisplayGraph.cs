﻿using ChartAndGraph;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeDisplayGraph : MonoBehaviour
{
    private const string slimeCountText = "Slimes";

    private float _time = 0f;
    private float _lastX = 0f;
    private int _lastYSlimes = 0;

    public GraphChart graph;
    public NavBarTexts NavBarTexts;
    public float interval = 1f;

    // Start is called before the first frame update
    void Start()
    {
        if (graph == null)
        {
            return;
        }
        graph.DataSource.ClearCategory(slimeCountText);

        _time = interval;
    }

    // Update is called once per frame
    void Update()
    {
        if (_time > 0)
        {
            _time -= Time.deltaTime;
            return;
        }

        _time = interval;

        if (_lastX == 0 && _lastYSlimes == 0)
        {
            graph.DataSource.AddPointToCategory(slimeCountText, 0, 0);
            graph.DataSource.AddPointToCategory(slimeCountText, 0, NavBarTexts.currentSlimes);

            _lastX += 1;
        }

        if(_lastYSlimes != NavBarTexts.currentSlimes)
        {
            graph.DataSource.AddPointToCategory(slimeCountText, _lastX, NavBarTexts.currentSlimes);

            _lastX += 1;
            _lastYSlimes = NavBarTexts.currentSlimes;
        } 
    }
}
