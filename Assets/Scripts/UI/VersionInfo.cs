﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionInfo : MonoBehaviour
{
    public Text versionText;

    // Update is called once per frame
    void Update()
    {
        versionText.text = $"Version: {Application.version}";
    }
}
