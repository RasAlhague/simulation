﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void OnPlayClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void OnOptionsClicked()
    {
        Debug.Log("Options clicked!");
    }

    public void OnQuitClicked()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }
}
