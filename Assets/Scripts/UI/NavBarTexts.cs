﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavBarTexts : MonoBehaviour
{
    private float _time = 1f;
    private int _highestGen = 0;
    private int _higestSlimeCount = 0;
    private int _currentSlimes = 0;

    public float updateInterval = 1f;

    public Text countSlimesText;
    public Text highestGenText;
    public Text highestSlimeCountText;

    public int currentSlimes = 0;
    public int higestSlimeCount = 0;
    public int highestGeneration = 0;

    void Update()
    {
        if (_time > 0)
        {
            _time -= Time.deltaTime;
            return;
        }

        _time = updateInterval;

        var objects = GameObject.FindGameObjectsWithTag("Slime");
        _currentSlimes = objects.Length;

        if (_currentSlimes > _higestSlimeCount)
        {
            _higestSlimeCount = _currentSlimes;
        }

        foreach (var o in objects)
        {
            var lifeForm = o.GetComponent<Lifeform>();
            if(_highestGen < lifeForm.generation)
            {
                _highestGen = lifeForm.generation;
            }
        }

        countSlimesText.text = $"Slimes: {_currentSlimes}";
        highestGenText.text = $"Highest generation: {_highestGen}";
        highestSlimeCountText.text = $"Higest slime count: {_higestSlimeCount}";

        currentSlimes = _currentSlimes;
    }
}
