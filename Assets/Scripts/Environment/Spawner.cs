﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private float time;
    private bool hasRun = false;

    public int spawnNumber = 10;
    public GameObject spawnObject;
    public bool randomize = false;
    public float interval = 10f;
    public bool oneTime = true;
    public float spawnRange = 1.0f;

    private void Start()
    {
        time = 0;
    }

    void Update()
    {
        if (oneTime && hasRun)
        {
            return;
        }

        if (time > 0)
        {
            time -= Time.deltaTime;
            return;
        }

        var genomes = GetComponents<GenomeBase>();

        for (int i = 0; i < spawnNumber; i++)
        {
            var go = Instantiate(spawnObject, new Vector3(transform.position.x + Random.Range(-spawnRange, spawnRange), transform.position.y + Random.Range(-spawnRange, spawnRange), 0), Quaternion.identity);
            var lifeform = go.GetComponent<Lifeform>();

            foreach (var genome in genomes)
            {
                var comp = (GenomeBase)go.AddComponent(genome.GetType());
                comp.GetValueFromOther(genome);

                if (randomize)
                {
                    comp.RandomizeValues();
                }

                lifeform.genomes.Add(comp);
            }
        }

        hasRun = true;
        time = interval;
    }
}
