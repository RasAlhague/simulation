﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    public float thirstReduction = 3;
    public float waterRate = 1f;
    private float time;

    public void Start()
    {
        time = waterRate;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Slime" && time <= 0)
        {
            collision.SendMessage("DecreaseThirst", thirstReduction);

            time = waterRate;
        }

        time -= Time.fixedDeltaTime;
    }
}
